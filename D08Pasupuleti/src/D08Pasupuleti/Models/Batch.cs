﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace D08Pasupuleti.Models
{
    public class Batch
    {
        [ScaffoldColumn(false)]
        [Key]
        public string BatchID { get; set; }

        [Required]
        [Display(Name = "Batch Name")]
        public String BatchName { get; set; }


        [RegularExpression(@"^\d{4}$", ErrorMessage = "Invalid Year")]
        [Display(Name = "Batch Year")]
        public int BatchYear { get; set; }


        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }

        [Display(Name = "Degree Level")]
        public string DegreeLevel { get; set; }

        [ScaffoldColumn(false)]
        public int? LocationID { get; set; }

        [Display(Name = "Batch Location")]
        public virtual Location Location { get; set; }


        public List<Location> filmSets { get; set; }

        public static List<Batch> ReadAllFromCSV(string filepath)
        {
            List<Batch> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Batch.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Batch OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Batch item = new Batch();

            int i = 0;
            item.BatchID = Convert.ToString(values[i++]);
            item.BatchName = Convert.ToString(values[i++]);
            item.BatchYear = Convert.ToInt32(values[i++]);
            item.DepartmentName = Convert.ToString(values[i++]);
            item.DegreeLevel = Convert.ToString(values[i++]);

            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
