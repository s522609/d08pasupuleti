﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using D08Pasupuleti.Models;
using System.IO;
using Microsoft.Extensions.DependencyInjection;

namespace D08Pasupuleti.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Batchs.RemoveRange(context.Batchs);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedBatchsFromCsv(relPath, context);
        }

        private static void SeedBatchsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "batch.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Batch.ReadAllFromCSV(source);
            List<Batch> lst = Batch.ReadAllFromCSV(source);
            context.Batchs.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
