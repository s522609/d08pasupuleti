using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Pasupuleti.Models;

namespace D08Pasupuleti.Controllers
{
    [Produces("application/json")]
    [Route("api/Batches")]
    public class BatchesController : Controller
    {
        private AppDbContext _context;

        public BatchesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Batches
        [HttpGet]
        public IEnumerable<Batch> GetBatchs()
        {
            return _context.Batchs;
        }

        // GET: api/Batches/5
        [HttpGet("{id}", Name = "GetBatch")]
        public IActionResult GetBatch([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Batch batch = _context.Batchs.Single(m => m.BatchID == id);

            if (batch == null)
            {
                return HttpNotFound();
            }

            return Ok(batch);
        }

        // PUT: api/Batches/5
        [HttpPut("{id}")]
        public IActionResult PutBatch(string id, [FromBody] Batch batch)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != batch.BatchID)
            {
                return HttpBadRequest();
            }

            _context.Entry(batch).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BatchExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Batches
        [HttpPost]
        public IActionResult PostBatch([FromBody] Batch batch)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Batchs.Add(batch);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BatchExists(batch.BatchID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetBatch", new { id = batch.BatchID }, batch);
        }

        // DELETE: api/Batches/5
        [HttpDelete("{id}")]
        public IActionResult DeleteBatch(string id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Batch batch = _context.Batchs.Single(m => m.BatchID == id);
            if (batch == null)
            {
                return HttpNotFound();
            }

            _context.Batchs.Remove(batch);
            _context.SaveChanges();

            return Ok(batch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BatchExists(string id)
        {
            return _context.Batchs.Count(e => e.BatchID == id) > 0;
        }
    }
}