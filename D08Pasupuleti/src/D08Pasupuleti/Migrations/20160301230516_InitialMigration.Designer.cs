using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Pasupuleti.Models;

namespace D08Pasupuleti.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160301230516_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Pasupuleti.Models.Batch", b =>
                {
                    b.Property<string>("BatchID");

                    b.Property<string>("BatchName")
                        .IsRequired();

                    b.Property<int>("BatchYear");

                    b.Property<string>("DegreeLevel");

                    b.Property<string>("DepartmentName");

                    b.Property<int?>("LocationID");

                    b.HasKey("BatchID");
                });

            modelBuilder.Entity("D08Pasupuleti.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("BatchBatchID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Pasupuleti.Models.Batch", b =>
                {
                    b.HasOne("D08Pasupuleti.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });

            modelBuilder.Entity("D08Pasupuleti.Models.Location", b =>
                {
                    b.HasOne("D08Pasupuleti.Models.Batch")
                        .WithMany()
                        .HasForeignKey("BatchBatchID");
                });
        }
    }
}
